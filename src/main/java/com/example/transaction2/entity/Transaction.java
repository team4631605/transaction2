package com.example.transaction2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(nullable = false)
    private Long sender_card_id;
    @Column(nullable = false)
    private Long receiver_card_id;

    @Column(nullable = false)
    private Long sender_amount;
    @Column(nullable = false)
    private Long receiver_amount;

    @Column(nullable = false)
    private String status;
    @CreationTimestamp
    private LocalDateTime created_at;
}
