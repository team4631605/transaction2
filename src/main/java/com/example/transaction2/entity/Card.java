package com.example.transaction2.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Long balance;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false,unique = true)
    private String cardNumber;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String currency;
    @CreatedBy()
    private UUID createdBy;

}
