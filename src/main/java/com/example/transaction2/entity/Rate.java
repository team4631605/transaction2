package com.example.transaction2.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
public class Rate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Long rate;
    @Column(nullable = false)
    private String to_currency;
    @Column(nullable = false)
    private String from_currency;
}
