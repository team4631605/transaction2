package com.example.transaction2.controller;

import com.example.transaction2.entity.User;
import com.example.transaction2.payload.CardDTO;
import com.example.transaction2.response.ApiResult;
import com.example.transaction2.security.CurrentUser;
import com.example.transaction2.service.CardService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/card")
@RequiredArgsConstructor
public class CardController {
    private final CardService cardService;
    @PostMapping("/add")
    public ApiResult<CardDTO> addCard(@Valid @RequestBody CardDTO cardDTO, @CurrentUser User user) {
        return cardService.add(cardDTO,user);
    }
}
