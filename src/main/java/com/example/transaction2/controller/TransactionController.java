package com.example.transaction2.controller;
import com.example.transaction2.entity.User;
import com.example.transaction2.payload.TransactionDTO;
import com.example.transaction2.response.ApiResult;
import com.example.transaction2.security.CurrentUser;
import com.example.transaction2.service.TransactionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth/transaction")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;
    @PostMapping("/add")
    public ApiResult<TransactionDTO> addCard(@Valid @RequestBody TransactionDTO transactionDTO, @CurrentUser User user) {
        return transactionService.add(transactionDTO,user);
    }
}
