package com.example.transaction2.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class CardDTO {
    @NotBlank
    private String name;
    @NotBlank
    private String cardNumber;
    @NotBlank
    private String type;
    @NotBlank
    private String currency;
}
