package com.example.transaction2.service;

import com.example.transaction2.entity.Card;
import com.example.transaction2.entity.Transaction;
import com.example.transaction2.entity.User;
import com.example.transaction2.payload.TransactionDTO;
import com.example.transaction2.repository.CardRepository;
import com.example.transaction2.repository.TransactionRepository;
import com.example.transaction2.response.ApiResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final CardRepository cardRepository;
    private final TransactionRepository transactionRepository;
    public ApiResult<TransactionDTO> add(TransactionDTO transactionDTO, User user) {
//        if (transactionRepository.existsByReceiver_card(transactionDTO.getReceiver_card_number())) {
//            throw RestException.restThrow("Invalid card number", HttpStatus.FORBIDDEN);
//        }
        Card sender_card = cardRepository.findByCardNumber(transactionDTO.getSender_card_number());
        Card receiver_card = cardRepository.findByCardNumber(transactionDTO.getReceiver_card_number());
        Long sender_card_id = sender_card.getId();
        Long receiver_card_id = receiver_card.getId();
        Transaction transaction = new Transaction();
        transaction.setSender_amount(transactionDTO.getSender_amount());
        transaction.setReceiver_card_id(receiver_card_id);
        transaction.setReceiver_amount(transactionDTO.getReceiver_amount());
        transaction.setSender_card_id(sender_card_id);
        transaction.setStatus("new");
        transactionRepository.save(transaction);
        confirm(transaction.getId());
        return ApiResult.successResponse();
    }

    private void confirm(UUID transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).get();
        Long senderCardId = transaction.getSender_card_id();
        Long receiverCardId = transaction.getReceiver_card_id();
        Card sender_card = cardRepository.findById(senderCardId).get();
        Card receiver_card = cardRepository.findById(receiverCardId).get();
        sender_card.setBalance(sender_card.getBalance()-transaction.getSender_amount());
        receiver_card.setBalance(receiver_card.getBalance()+transaction.getReceiver_amount());
        cardRepository.save(sender_card);
        cardRepository.save(receiver_card);
    }
}
