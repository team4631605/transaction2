package com.example.transaction2.service;

import com.example.transaction2.entity.Card;
import com.example.transaction2.entity.User;
import com.example.transaction2.exception.RestException;
import com.example.transaction2.payload.CardDTO;
import com.example.transaction2.repository.CardRepository;
import com.example.transaction2.repository.UserRepository;
import com.example.transaction2.response.ApiResult;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {
    private final CardRepository cardRepository;
    private final UserRepository userRepository;

    public ApiResult<CardDTO> add(CardDTO cardDTO,User user) {
        if(cardRepository.existsByCardNumber(cardDTO.getCardNumber()))
            throw RestException.restThrow("Card number already existed",HttpStatus.FORBIDDEN);
        Card card = new Card();
        card.setName(cardDTO.getName());
        card.setCardNumber(cardDTO.getCardNumber());
        card.setCurrency(cardDTO.getCurrency());
        card.setType(cardDTO.getType());
        card.setCreatedBy(userRepository.findById(user.getId()).get().getId());
        cardRepository.save(card);
        return ApiResult.successResponse();
    }
}
