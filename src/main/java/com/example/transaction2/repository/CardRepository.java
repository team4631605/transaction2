package com.example.transaction2.repository;

import com.example.transaction2.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card,Long> {
    boolean existsByCardNumber(String cardNumber);
     Card findByCardNumber(String cardNUmber);
}
